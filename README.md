A simple REST API for adding/accessing users:
Requires a MongoDB database at port 27017

Method    | Request URL        | Result
-----------------------------------------------
GET	      | /api/v1/users/     | List all users
GET	      | /api/v1/users/{id} | Returns the user with the given UUID
POST	  | /api/v1/users/     | Adds a new user
POST	  | /api/v1/users/{id} | Updates an existing user with the given UUID
DELETE	  | /api/v1/users/{id} | Deletes an existing user with the given UUID
