package com.example.videoteltest.converters;

import com.example.videoteltest.domain.User;
import com.example.videoteltest.datatransfer.UserInput;
import com.example.videoteltest.services.NationalityLookupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UserInputToUser implements Converter<UserInput, User> {
    private static final int ISO_CODE_LENGTH = 3;

    private NationalityLookupService nationalityLookupService;

    @Autowired
    public UserInputToUser(NationalityLookupService nationalityLookupService) {
        this.nationalityLookupService = nationalityLookupService;
    }

    @Override
    public User convert(UserInput userInput) {
        return convertWithId(UUID.randomUUID(), userInput);
    }

    public User convertWithId(UUID id, UserInput userInput) {
        String country = limitCountryLength(userInput.getCountry());

        return User.builder()
                .id(id)
                .firstName(userInput.getFirstName())
                .lastName(userInput.getLastName())
                .dateOfBirth(userInput.getDateOfBirth())
                .country(country)
                .nationality(lookupNationality(country))
                .build();
    }

    private String limitCountryLength(String country) {
        boolean isCountryTooLong = country != null && country.length() > ISO_CODE_LENGTH;
        return isCountryTooLong ? country.substring(0, ISO_CODE_LENGTH) : country;
    }

    private String lookupNationality(String country) {
        return nationalityLookupService.getNationalityFromIsoCode(country);
    }

}
