package com.example.videoteltest.datatransfer;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

public class UserInput {
    @Getter @Setter
    private String firstName;

    @Getter @Setter
    private String lastName;

    @Getter @Setter
    private Date dateOfBirth;

    @Getter @Setter
    private String country;
}
