package com.example.videoteltest.datatransfer;


import lombok.Getter;
import lombok.Setter;

public class RestCountry {
    @Getter @Setter
    private String demonym;
}
