package com.example.videoteltest.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.UUID;

@Builder
@Document
public class User {
    @Id
    @Getter
    final private UUID id;

    @Getter @Setter
    private String firstName;

    @Getter @Setter
    private String lastName;

    @Getter @Setter
    private Date dateOfBirth;

    @Getter @Setter
    private String country;

    @Getter @Setter
    private String nationality;
}
