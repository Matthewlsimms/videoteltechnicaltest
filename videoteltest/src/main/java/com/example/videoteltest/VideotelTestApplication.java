package com.example.videoteltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideotelTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(VideotelTestApplication.class, args);
	}

}
