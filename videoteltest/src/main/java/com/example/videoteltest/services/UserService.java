package com.example.videoteltest.services;

import com.example.videoteltest.domain.User;
import com.example.videoteltest.datatransfer.UserInput;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {
    List<User> findAll();
    Optional<User> findById(UUID id);
    User save(UserInput userInput);
    User saveWithId(UUID id, UserInput userInput);
    void delete(UUID id);
}
