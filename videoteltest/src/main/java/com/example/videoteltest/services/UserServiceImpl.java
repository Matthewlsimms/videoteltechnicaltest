package com.example.videoteltest.services;

import com.example.videoteltest.converters.UserInputToUser;
import com.example.videoteltest.domain.User;
import com.example.videoteltest.datatransfer.UserInput;
import com.example.videoteltest.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private UserInputToUser userInputToUser;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserInputToUser userInputToUser) {
        this.userRepository = userRepository;
        this.userInputToUser = userInputToUser;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach(users::add);
        return users;
    }

    @Override
    public Optional<User> findById(UUID id) {
        return userRepository.findById(id);
    }

    @Override
    public User save(UserInput userInput) {
        User user = userInputToUser.convert(userInput);
        return userRepository.save(user);
    }

    @Override
    public User saveWithId(UUID id, UserInput userInput) {
        User user = userInputToUser.convertWithId(id, userInput);
        return userRepository.save(user);
    }

    @Override
    public void delete(UUID id) {
        userRepository.deleteById(id);
    }
}
