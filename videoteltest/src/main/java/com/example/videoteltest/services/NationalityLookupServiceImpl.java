package com.example.videoteltest.services;

import com.example.videoteltest.datatransfer.RestCountry;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class NationalityLookupServiceImpl implements NationalityLookupService {

    final private String baseUri = "https://restcountries.eu/rest/v2/alpha/";
    final private String fieldFilter = "?fields=demonym";

    @Override
    public String getNationalityFromIsoCode(String isoCode) {
        String uri = new StringBuilder(baseUri).append(isoCode).append(fieldFilter).toString();

        RestTemplate restTemplate = new RestTemplate();
        try {
            RestCountry restCountry = restTemplate.getForObject(uri, RestCountry.class);
            if (restCountry != null) {
                return restCountry.getDemonym();
            }
        } catch (RestClientException e) {
            e.printStackTrace();
        }
        return "Invalid ISO code";
    }
}
