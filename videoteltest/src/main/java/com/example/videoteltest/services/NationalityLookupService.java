package com.example.videoteltest.services;

public interface NationalityLookupService {
    String getNationalityFromIsoCode(String isoCode);
}
